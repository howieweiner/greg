const sharp = require("sharp");
const TextToSVG = require('text-to-svg');
const textToSVG = TextToSVG.loadSync();

const attributes = {fill: 'black', stroke: 'black'};
const options = {x: 0, y: 0, fontSize: 24, anchor: 'top', attributes: attributes};

const mix = face => {
  const text = face.replace('_', ' ').to;
  const svgText = Buffer.from(textToSVG.getSVG(text, options));

  return sharp("./images/generic-banner.jpg")
    .composite([{input: `./images/${face}.png`}, {input: svgText, top: 20, left: 300}])
    .png()
    .toBuffer();
}

module.exports = {mix};