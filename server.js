const express = require('express');
const server = express();
const mixer = require('./mixer');

server.get('/banner', (req, res) => {
    res.type('image/png');

    const face = req.query.face;

    mixer.mix(face).then(data => {
        res.status(200).send(data);
    }).catch(err => {
        console.error(err);
        res.status(500).send();;
    });
});

server.listen(8000, () => {
  console.log('Server started!')
});
